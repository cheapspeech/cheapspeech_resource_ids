export default class Manifest {
    restAddresses: Array<RestContainer> = []
    fileShardAddresses: Array<FileShard> = []
    activeHashSchemas: Array<number> = []
    targetHashSchema: number = 1

    static fromJson(json: any): Manifest {
        var newManifest: Manifest = new Manifest()

        if (json == null) {
            return newManifest;
        }

        if (json.hasOwnProperty("restAddresses")) {
            newManifest.restAddresses = json.restAddresses
        }

        if (json.hasOwnProperty("fileShardAddresses")) {
            newManifest.fileShardAddresses = json.fileShardAddresses
        }

        if (json.hasOwnProperty("activeHashSchemas")) {
            newManifest.activeHashSchemas = json.activeHashSchemas
        }

        if (json.hasOwnProperty("targetHashSchema")) {
            newManifest.targetHashSchema = json.targetHashSchema
        }

        return newManifest
    }

    toJson(): string {
        return JSON.stringify({
            restAddresses: this.restAddresses,
            fileShardAddresses: this.fileShardAddresses,
            activeHashSchemas: this.activeHashSchemas,
            targetHashSchema: this.targetHashSchema
        })
    }

    addFileShard(address: string, port: string, shardNumber: number) {
        this.fileShardAddresses.push(new FileShard(address, port, shardNumber))
    }

    addRestContainer(address: string, port: string) {
        this.restAddresses.push(new RestContainer(address, port))
    }
}

class FileShard {
    address: string = ""
    port: string = ""
    shardNumber: number = -1
    failedCalls: number = 0

    constructor(address: string, port: string, shardNumber: number) {
        this.address = address
        this.port = port
        this.shardNumber = shardNumber
    }
}

class RestContainer {
    address: string = ""
    port: string = ""
    failedCalls: number = 0

    constructor(address: string, port: string) {
        this.address = address
        this.port = port
    }
}