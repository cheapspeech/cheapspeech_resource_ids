import crypto from "crypto";

const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-"
const idLength = 28
const bitsPerSpace = 64
const bytesToGenerate = 21
const totalPossibleFiles = Math.pow(bitsPerSpace, idLength);

export default class ResourceId {
    shardLimits: Array<number> = []

    getNewId(): string {
        return crypto.randomBytes(Math.floor(bytesToGenerate)).toString("base64url")
    }

    getPotentialLocations(fileName: string, schemas: Array<number> | undefined): Array<number> {
        if (!schemas) {
            return []
        }

        var locations = []
        for (var schema of schemas) {
            locations.push(this.getShardNumber(fileName, schema))
        }

        return locations
    }

    getShardNumber(hash: string, numShards: number): number {
        var b64: number = this.base64ToInt(hash)
        if (this.shardLimits.length == 0) {
            this.shardLimits = this.getShardLimits(numShards)
        }

        var index: number = 0
        var start: number = 0
        var end: number = 0

        do {
            end = this.shardLimits[index]
            if (b64 >= start && b64 <= end) {
                return index;
            }

            start = end
            index++
        }
        while (index < numShards)

        return index
    }

    base64ToInt(str: string): number {
        var sum: number = 0
        var index: number = 0
        var value: number = 0
        Array.from(str).forEach((elem) => {
            value = chars.indexOf(elem)
            sum += value * Math.pow(bitsPerSpace, index)
            index++
        })

        return sum;
    }

    intToBase64(num: number): string {
        var remainders: Array<number> = []
        var quotient: number = 0
        do {
            quotient = Math.floor(num / bitsPerSpace)
            remainders.push(num % bitsPerSpace)
            num = quotient
        }
        while (quotient != 0)

        var str: string = remainders.map((elem) => chars[elem]).join("")
        return str
    }

    getShardLimits(numShards: number): Array<number> {
        var shardLimits: Array<number> = []
        for (var i = 1; i < numShards + 1; i++) {
            shardLimits.push(Math.floor(totalPossibleFiles) * i / numShards)
        }

        return shardLimits;
    }
}